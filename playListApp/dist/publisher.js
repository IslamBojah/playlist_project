"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const RabbitMq_1 = require("./rabbitmq/RabbitMq");
class App {
    constructor() {
        this.rabbitMq = new RabbitMq_1.RabbitMq('amqp://localhost:5672', 'QueueName');
        this.app = express_1.default();
        this.config();
        this.rabbitMq.sendMessage({
            "rating": "4.3/5",
            "normalized_rating": 4.3,
            "shop_uniqueness": "2020-01-14:2020-01-15/2020-01-18/13523/us>not_passed",
        });
    }
    config() {
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(cors_1.default());
    }
}
exports.default = new App().app;
