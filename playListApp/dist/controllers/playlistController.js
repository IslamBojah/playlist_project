"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const playlist_model_1 = require("../models/playlist_model");
const PlayList = mongoose_1.default.model('playlist', playlist_model_1.playList_schema);
class playlist_controller {
    addNewPlayList(req, res) {
        console.log(req.body);
        let new_playlist = new PlayList(req.body);
        new_playlist.save((err, result) => {
            if (err)
                res.status(200).json({ success: false });
            else
                res.status(200).json(result);
        });
    }
    getAllPlayList(req, res) {
        PlayList.find({}, (err, result) => {
            if (err)
                res.status(200).json({ success: false });
            else
                res.status(200).json(result);
        });
    }
    updatePlaylist(req, res) {
        PlayList.findByIdAndUpdate({ _id: req.query.id }, req.body, (err, result) => {
            if (err)
                res.status(200).json({ success: false });
            else
                res.status(200).json(result);
        });
    }
    deleteById(req, res) {
        PlayList.findByIdAndDelete({ _id: req.query.id }, (err, result) => {
            if (err)
                res.status(200).json({ success: false });
            else
                res.status(200).json(result);
        });
    }
}
exports.playlist_controller = playlist_controller;
