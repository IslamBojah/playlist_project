"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const message_model_1 = require("../models/message_model");
const Message_Data = mongoose_1.default.model('message', message_model_1.Message_schema);
class message_controller {
    addNewmessage(req) {
        console.log(req);
        let new_message = new Message_Data(req);
        new_message.save((err, result) => {
            if (err)
                console.log(err);
            else
                console.log(result);
        });
    }
}
exports.message_controller = message_controller;
