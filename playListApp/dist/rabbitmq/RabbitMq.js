"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const amqp = __importStar(require("amqplib/callback_api"));
const message_controller_1 = require("../controllers/message_controller");
class RabbitMq {
    constructor(url, queueName) {
        this.message_con = new message_controller_1.message_controller();
        this.queue = queueName;
        this.conn = new Promise((resolve) => {
            resolve(this.connect(url));
        });
        this.channel = new Promise((resolve) => {
            resolve(this.createChannel(this.queue));
        });
    }
    connect(url = 'amqp://localhost:5672') {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                amqp.connect(url, (error, connection) => __awaiter(this, void 0, void 0, function* () {
                    if (error)
                        reject(error);
                    else
                        resolve(connection);
                }));
            });
        });
    }
    createChannel(queueName) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.conn.then((connection) => {
                    connection.createChannel((error, channel) => {
                        if (error)
                            reject(error);
                        else {
                            channel.assertQueue(queueName);
                            resolve(channel);
                        }
                    });
                });
            });
        });
    }
    sendMessage(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            (yield this.channel).sendToQueue(this.queue, Buffer.from(JSON.stringify(msg)));
            console.log("publisher sent ", msg);
        });
    }
    reciveMessage() {
        return __awaiter(this, void 0, void 0, function* () {
            //console.log('hello')
            (yield this.channel).consume(this.queue, (message) => __awaiter(this, void 0, void 0, function* () {
                const msg = message ? JSON.parse(message.content.toString()) : {};
                yield this.message_con.addNewmessage(msg);
            }), {
                noAck: true
            });
        });
    }
}
exports.RabbitMq = RabbitMq;
