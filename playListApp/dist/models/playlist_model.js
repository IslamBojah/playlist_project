"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
exports.playList_schema = new Schema({
    playlist_name: {
        type: String,
        required: true
    },
    singer_name: {
        type: String,
        required: true
    },
    songs_list: [{
            type: String,
            required: true
        }]
});
