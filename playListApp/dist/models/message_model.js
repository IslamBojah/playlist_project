"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
exports.Message_schema = new Schema({
    rating: {
        type: String,
    },
    normalized_rating: {
        type: Number,
    },
    shop_uniqueness: {
        type: String,
    }
});
