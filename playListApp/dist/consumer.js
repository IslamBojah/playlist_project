"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const RabbitMq_1 = require("./rabbitmq/RabbitMq");
class App {
    constructor() {
        this.rabbitMq = new RabbitMq_1.RabbitMq('amqp://localhost:5672', 'QueueName');
        this.app = express_1.default();
        this.config();
        this.mongoSetup();
        this.rabbitMq.reciveMessage();
    }
    config() {
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(cors_1.default());
    }
    mongoSetup() {
        mongoose_1.default.connect("mongodb://localhost/message", { useNewUrlParser: true, useUnifiedTopology: true })
            .then(() => console.log('Connected to MongoDB...'))
            .catch(err => console.error('Could not connect to MongoDB...' + err));
    }
}
exports.default = new App().app;
