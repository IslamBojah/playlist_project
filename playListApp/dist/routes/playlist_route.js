"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const playlistController_1 = require("../controllers/playlistController");
class PlayListRoute {
    constructor() {
        this.playlist_controller = new playlistController_1.playlist_controller();
    }
    routes(app) {
        app.route('/')
            .get(this.playlist_controller.getAllPlayList)
            .post(this.playlist_controller.addNewPlayList)
            .put(this.playlist_controller.updatePlaylist)
            .delete(this.playlist_controller.deleteById);
    }
}
exports.PlayListRoute = PlayListRoute;
