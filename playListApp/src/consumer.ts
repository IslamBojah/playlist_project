import express, { Application } from 'express';
import mongoose from 'mongoose'
import cors from 'cors'
import {RabbitMq} from './rabbitmq/RabbitMq'

class App {
    public app: Application;
    public rabbitMq:RabbitMq = new RabbitMq('amqp://localhost:5672' , 'QueueName');
    constructor() {
        this.app = express();
        this.config()
        this.mongoSetup();
        this.rabbitMq.reciveMessage();
    }
    
    private config(): void {
        this.app.use(express.json())
        this.app.use(express.urlencoded({extended:true}))
        this.app.use(cors());
    }
    private mongoSetup(): void {
        mongoose.connect("mongodb://localhost/message", { useNewUrlParser: true, useUnifiedTopology: true })
            .then(() => console.log('Connected to MongoDB...'))
            .catch(err => console.error('Could not connect to MongoDB...' + err))
    }
}
export default new App().app