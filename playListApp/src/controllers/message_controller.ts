import mongoose from 'mongoose'
import { Message_schema } from '../models/message_model'
import { CompSet_schema } from '../models/compSet'
import * as amqp from 'amqplib/callback_api'

const Message_Data = mongoose.model('message', Message_schema)
const compSet_Data = mongoose.model('compSet', CompSet_schema)

export class message_controller {
    public addNewmessage(req: amqp.Message) {
        let new_message = new Message_Data(req);
        new_message.save((err, result) => {
            if (err) console.log(err);
            // else console.log(result);
        })
    }

    public async get_relevernt_compSet(req: Number) {
        const result = await compSet_Data.findOne({
            $or: [
                { "customer_hotel_id": req },
                { "competitors": req }
            ]
        }, (err, result) => {
            return result
            console.log(result)
        })
        return result
    }
}