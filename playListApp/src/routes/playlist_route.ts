import {Request , Response} from 'express'
import {playlist_controller} from '../controllers/playlistController'

export class PlayListRoute{
    public playlist_controller = new playlist_controller() ;
    public routes(app:any):void{
        app.route('/')
        .get(this.playlist_controller.getAllPlayList)
        .post(this.playlist_controller.addNewPlayList)
        .put(this.playlist_controller.updatePlaylist)
        .delete(this.playlist_controller.deleteById)
    }
}   