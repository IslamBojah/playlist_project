import express, { Application } from 'express';
import cors from 'cors'
import { RabbitMq } from './rabbitmq/RabbitMq'

class App {
    public app: Application;
    public rabbitMq: RabbitMq = new RabbitMq('amqp://localhost:5672', 'QueueName');
    constructor() {
        this.app = express();
        this.config();
        this.rabbitMq.sendMessage({
            "rating": "4.3/5",
            "normalized_rating": 4.3,
            "scan_id": 7593615,
            "shop_uniqueness": "2020-01-14:2020-01-15/2020-01-18/13523/us>not_passed",
            "image_url": "http://d3bu013za5934b.cloudfront.net/7593615/2uvedg_5_expedia_dimming_1585545334_20200115_002722_p1.png",
            "deep_link": "https://www.expedia.com/Knoxville-Hotels-Hilton-Knoxville-Airport.h7589.Hotel-Information?chkin=1%2F15%2F2020&chkout=1%2F18%2F2020&regionId=5462356&destType=MARKET&destination=Knoxville%2C+TN%2C+United+States+%28TYS-McGhee+Tyson%29&swpToggleOn=true&rm1=a2&x_pwa=1&sort=recommended&top_dp=149&top_cur=USD&rfrr=HSR&pwa_ts=1579047703558&neighborhoodId=null&referrerUrl=aHR0cHM6Ly93d3cuZXhwZWRpYS5jb20vSG90ZWwtU2VhcmNo",
            "is_deal_offer": "0",
            "CUG": false,
            "hotel_name": "Hilton Knoxville Airport, TN",
            "fornovaId": 13523,
            "region": "Americas",
            "proxy": "US",
            "hotel_id": "7589",
            "scan_date": "2020-01-14T00:00:00.000Z",
            "check_in_date": "2020-01-15T00:00:00.000Z",
            "check_out_date": "2020-01-18T00:00:00.000Z",
            "city": "Alcoa",
            "country": "Usa",
            "language": "en-us",
            "LOS": 3,
            "customer_name": "Hilton",
            "market": "Alcoa, Usa",
            "number_of_guests": "2",
            "scan_type": "market",
            "source_type": "OTA",
            "population_timestamp": "2020-01-20T09:15:55.611Z",
            "marketId": 24954,
            "position_in_page": 6,
            "position": 6,
            "availability": "",
            "source": "Expedia",
            "page": 1,
            "is_dimmed": 0,
            "cancellation_policy": "",
            "room_type": "",
            "price_for_comparison": 0,
            "total_price": 0,
            "calculatedData": { "rankingScore": 95 }
        });
    }
    private config(): void {
        this.app.use(express.json())
        this.app.use(express.urlencoded({ extended: true }))
        this.app.use(cors());
    }
}

export default new App().app