import * as amqp from "amqplib/callback_api";
import { message_controller } from '../controllers/message_controller'

export class RabbitMq {
    public conn: Promise<amqp.Connection>;
    public channel: Promise<amqp.Channel>
    public message_con: message_controller = new message_controller();
    public queue : string ;
    constructor(url : string , queueName : string ) {
        this.queue = queueName
        this.conn = new Promise((resolve) => {
            resolve(this.connect(url))
        });
        this.channel = new Promise((resolve) => {
            resolve(this.createChannel(this.queue))
        })        
    }
    private async connect(url: string = 'amqp://localhost:5672'): Promise<amqp.Connection> {
        return new Promise<amqp.Connection>((resolve, reject) => {
            amqp.connect(url, async (error: any, connection) => {
                if (error) reject(error)
                else resolve(connection)
            })
        })
    }
    private async createChannel(queueName:string): Promise<amqp.Channel> {
        return new Promise<amqp.Channel>((resolve, reject) => {
            this.conn.then((connection) => {
                connection.createChannel((error, channel) => {
                    if (error) reject(error)
                    else {
                        channel.assertQueue(queueName)
                        resolve(channel)
                    }
                })
            })
        })
    }
    public async sendMessage( msg: any) {
        (await this.channel).sendToQueue(this.queue, Buffer.from(JSON.stringify(msg)))
        console.log("publisher sent " , msg)
    }
    public async reciveMessage(){
        //console.log('hello')
        (await this.channel).consume(this.queue ,async(message : amqp.Message | null)=>{
            const msg = message? JSON.parse(message.content.toString()) : {}
            const res = await this.message_con.get_relevernt_compSet(msg.fornovaId)
            console.log(res , "result")
        },{
            noAck: true
        })
    }
}