import mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const CompSet_schema = new Schema({
    customer_id : {
        type: String
    } ,
    compset_type:{
        type: String
    } ,
    customer_hotel_id:{
        type: Number
    } ,
    pos:{
        type: String
    },
    providers:{
        type: Array
    },
    competitors:{
        type: Array
    },
    marketId:{
        type: Number
    } ,
    los:{
        type: Number
    }

})

