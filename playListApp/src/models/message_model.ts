import mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const Message_schema = new Schema({
    rating: {
        type: String,
      //  required: true
    },
    normalized_rating: {
        type: Number,
      //  required: true
    },
    scan_id:{
        type:Number , 
    },
    shop_uniqueness: {
        type: String,
       // required: true
    }, 
    image_url:{
        type: String
    },
    deep_link : {
        type: String
    },
    is_deal_offer: {
        type: String
    },
    CUG: {
        type: Boolean
    },
    hotel_name : {
        type: String
    },
    fornovaId : {
        type: Number
    },
    region: {
        type: String
    },
    proxy : {
        type: String
    },
    hotel_id: {
        type: String
    },
    scan_date: {
        type: String
    },
    check_in_date : {
        type: String
    },
    check_out_date: {
        type: String
    },
    city: {
        type: String
    },
    country: {
        type: String
    },
    language: {
        type: String
    },
    LOS: {
        type: Number
    },
    customer_name: {
        type: String
    },
    market: {
        type: String
    },
    number_of_guests: {
        type: String
    },
    scan_type: {
        type: String
    },
    source_type: {
        type: String
    },
    population_timestamp: {
        type: String
    },
    marketId: {
        type: Number
    },
    position_in_page: {
        type: Number
    },
    position: {
        type: Number
    },
    availability: {
        type: String
    },
    source: {
        type: String
    },
    page: {
        type: Number
    },
    is_dimmed: {
        type: Number
    },
    cancellation_policy :{
        type: String
    },
    room_type: {
        type: String
    },
    price_for_comparison: {
        type: Number
    },
    total_price: {
        type: Number
    },
    calculatedData:{
        rankingScore:{
            type:Number
        }
    }

})

